Installation
============

Requirements
------------

* Python 3.10 or higher

Installing mets
---------------

You can install mets directly from the source code:

1. Clone the repository:

   .. code-block:: bash

      git clone https://gitlab.com/cgs2824870/mets.git
      cd mets

2. Install the package:

   .. code-block:: bash

      pip install -e .

This will install mets in editable mode, allowing you to make changes to the source code that will be immediately reflected in your installed version.

Installing development dependencies
-----------------------------------

If you want to contribute to the project or run tests, you should install the development dependencies:

.. code-block:: bash

   pip install -e .[test]

This will install additional packages required for testing and development.
