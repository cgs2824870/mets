Welcome to mets documentation!
==============================

mets (MetroSearch) is a command-line interface (CLI) tool for searching and retrieving data from the Metropolitan Museum of Art's public API.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
   api
   contributing
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
