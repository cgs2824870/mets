Usage
=====

Basic usage
-----------

After installing mets, you can use it from the command line:

.. code-block:: bash

   mets <search_term> [options]

Options
-------

- `-i`, `--images`: Only return objects with images. Default: True
- `-n`, `--num`: Maximum number of results to return. Default: 80
- `-s`, `--sort`: Sort results by date (0: ascending, 1: descending, other: no sort). Default: 0
- `-o`, `--output`: Output JSON filename. Prints to stdout if not provided.
- `-m`, `--time`: Delay between API requests in seconds. Default: 0.001
- `-d`, `--download`: Download object images
- `-p`, `--path`: Folder to save downloaded images. Default: 'images'
- `-t`, `--title`: Search in title only
- `-g`, `--tags`: Search in tags only

Example
-------

To search for objects containing "sunflower", return up to 5 results sorted by date descending, save the output to `out.json`, and download the associated images:

.. code-block:: bash

   mets "sunflower" -n 5 -s 1 -o out.json -d

This will create a JSON file named `out.json` with the search results and download the images to a folder named `images` in your current directory.
