API Reference
=============

MetroSearch
-----------

.. autoclass:: mets.MetroSearch
   :members:
   :undoc-members:
   :show-inheritance:

MetroCLI
--------

.. autoclass:: mets.MetroCLI
   :members:
   :undoc-members:
   :show-inheritance:
