Contributing
============

We welcome contributions to mets! This document outlines the process for contributing to this project.

Setting up your development environment
---------------------------------------

1. Fork the repository on GitLab.
2. Clone your fork locally:

   .. code-block:: bash

      git clone https://gitlab.com/your-username/mets.git
      cd mets

3. Create a virtual environment and install the development dependencies:

   .. code-block:: bash

      python -m venv venv
      source venv/bin/activate  # On Windows, use `venv\Scripts\activate`
      pip install -e .[test]

Running tests
-------------

We use pytest for our test suite. To run the tests:

.. code-block:: bash

   pytest

Coding standards
----------------

We follow PEP 8 for code style. Please ensure your code adheres to these standards.

We use Black for code formatting. Before submitting a pull request, please run:

.. code-block:: bash

   black src/mets

Submitting changes
------------------

1. Create a new branch for your changes:

   .. code-block:: bash

      git checkout -b name-of-your-bugfix-or-feature

2. Make your changes and commit them:

   .. code-block:: bash

      git commit -m "Your detailed description of your changes."

3. Push your branch to GitLab:

   .. code-block:: bash

      git push origin name-of-your-bugfix-or-feature

4. Submit a pull request through the GitLab website.

Thank you for your contributions!
