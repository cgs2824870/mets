Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
------------

Added
^^^^^
- Initial project structure
- Basic functionality for searching the Metropolitan Museum of Art API
- Command-line interface for easy interaction
- Asynchronous image downloading capability

[0.1.0] - 2024-03-01
--------------------

Added
^^^^^
- Initial release of mets
- Search functionality for Metropolitan Museum of Art objects
- Ability to download images associated with search results
- Command-line interface for easy interaction with the tool
